package route

import (
	"fmt"
	"l31/dal"
	"l31/model"

	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
)

type cold struct {
}

func (m cold) Add(ctx context.Context) {
	var op model.Cold
	ctx.ReadJSON(&op)
	fmt.Print(op)
	rt := dal.Cold.Add(op)
	println(rt)
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(map[string]interface{}{"success": rt == "success", "rt": rt})
}
func (m cold) Auto(ctx context.Context) {
	var op model.Auto
	ctx.ReadJSON(&op)
	fmt.Print(op)
	rt := dal.Cold.Auto(op)
	println(rt)
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(map[string]interface{}{"success": rt == "success", "rt": rt})
}
func (m cold) Noplst(ctx context.Context) {
	var op model.Auto
	ctx.ReadJSON(&op)
	fmt.Println(op)
	ds, total, rt := dal.Cold.Noplst(op.Nop)
	fmt.Println(ds)
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(map[string]interface{}{"success": rt == "success", "ds": ds, "total": total})
}
