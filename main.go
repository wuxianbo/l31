package main

import (
	"l31/route"

	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"github.com/kataras/iris/middleware/logger"
)

func main() {
	app := iris.New()
	app.RegisterView(iris.HTML("./views", ".html").Delims("{{{", "}}}").Reload(true))
	clog := logger.New(logger.Config{
		Status: true,
		IP:     true,
		Method: true,
		Path:   true,
	})
	app.Use(clog)
	app.Get("/", func(ctx context.Context) {
		ctx.View("index.html")
	})

	// app.Use(func(ctx context.Context) {
	// 	ctx.Application().Log("Begin request for path: %s", ctx.Path())
	// 	ctx.Next()
	// })
	// app.OnError(iris.StatusNotFound, func(ctx context.Context) {
	// 	ctx.HTML(iris.StatusNotFound, "<h2> 404 not found!</h2>")
	// })

	app.StaticWeb("/statics", "./statics/")
	app.Post("/addnum", route.Cold.Add)
	app.Post("/auto", route.Cold.Auto)
	app.Post("/lstnum", route.Cold.Noplst)
	app.Run(iris.Addr(":8090"))
}
