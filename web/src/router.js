import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function load(component) {
    // '@' is aliased to src/components
    return () =>
        import (`@/${component}.vue`)
}
let routes = [
    { path: '/', component: load('lst') },
    { path: '/get', component: load('Get') },
    // { path: '/select', component: load('Select') },
    //{ path: '/ana', component: load('HotColdQuery') },
    {
        path: '/lst',
        component: load('lst'),
        children: [{
            path: 'hand',
            component: load('lst/getcold/hand')
        }, {
            path: 'auto',
            component: load('lst/getcold/auto')
        }, {
            path: 'select',
            component: load('lst/HotColdQuery/select')
        }, {
            path: 'HotColdQuery',
            component: load('lst/HotColdQuery/HotColdQuery')
        }]
    },
    { path: '*', component: load('Error404') } // Not found
]

const Router = new VueRouter({
    mode: 'hash',
    scrollBehavior: () => ({ y: 0 }),
    routes
})

export default Router