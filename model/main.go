package model

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

type Cold struct {
	ID  uint64 `json:"id"`
	Nop string `json:"nop"`
	Se  string `json:"se"`
	D1  string `json:"d1"`
	D2  string `json:"d2"`
	D3  string `json:"d3"`
	D4  string `json:"d4"`
	D5  string `json:"d5"`
	D6  string `json:"d6"`
	D7  string `json:"d7"`
}
type Auto struct {
	ID  uint64 `json:"id"`
	Nop string `json:"nop"`
}
type Cfgm struct {
	Version   string
	Boltdb    string
	Sessiondb string
	Qqwrydb   string
	Port      int
}

var (
	conf = kingpin.Flag("conf", "config file default conf/config.toml").Default("conf/config.toml").Short('c').String()
)
var Cfg Cfgm

func init() {
	kingpin.Version("0.0.0.1")
	kingpin.Parse()
	cfgpath, _ := filepath.Abs(*conf)
	if _, err := toml.DecodeFile(cfgpath, &Cfg); err != nil {
		fmt.Println(err)
		return
	}
	Cfg.Boltdb = strings.Replace(cfgpath, "conf/config.toml", Cfg.Boltdb, -1)
	Cfg.Sessiondb = strings.Replace(cfgpath, "conf/config.toml", Cfg.Sessiondb, -1)
	Cfg.Qqwrydb = strings.Replace(cfgpath, "conf/config.toml", Cfg.Qqwrydb, -1)
}
