package dal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"l31/model"

	"github.com/boltdb/bolt"
)

type cold struct {
}

//排序函数
// type SavenumById []model.Savanum

// func (a SavenumById) Len() int {
// 	return len(a)
// }
// func (a SavenumById) Swap(i, j int) {
// 	a[i], a[j] = a[j], a[i]
// }
// func (a SavenumById) Less(i, j int) bool {
// 	return a[i].ID < a[j].ID
// }

//byord
// type TeamByord []model.Team

// func (a TeamByord) Len() int {
// 	return len(a)
// }
// func (a TeamByord) Swap(i, j int) {
// 	a[i], a[j] = a[j], a[i]
// }
// func (a TeamByord) Less(i, j int) bool {
// 	return a[i].Ord < a[j].Ord
// }
func (cold) Lst(start, limit int) (qrs []model.Cold, total int, rt string) {
	db, err := bolt.Open(model.Cfg.Boltdb, 0666, nil)
	if err != nil {
		rt = "数据库打开错误！"
		return
	}
	defer db.Close()
	rs := []model.Cold{}
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("Savenum"))
		if err != nil {
			return err
		}
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {

			var r model.Cold
			err = json.Unmarshal(v, &r)
			if err != nil {
				return err
			}
			rs = append(rs, r)
		}
		return nil
	})
	total = len(rs)
	rc := len(rs)

	if start > rc-1 || start < 0 {
		start = 0
	}
	last := start + limit
	if last > rc || last < start {
		last = rc
	}
	qrs = rs[start:last]

	rt = "success"
	return
}
func (cold) Noplst(nop string) (qrs []model.Cold, total int, rt string) {
	db, err := bolt.Open(model.Cfg.Boltdb, 0666, nil)
	if err != nil {
		rt = "数据库打开错误！"
		return
	}
	defer db.Close()
	rs := []model.Cold{}
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("Savenum"))
		if err != nil {
			return err
		}
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {

			var r model.Cold
			//print("test")
			err = json.Unmarshal(v, &r)
			if err != nil {
				return err
			}
			if r.Nop == nop {
				rs = append(rs, r)
			}

		}
		return nil
	})
	total = len(rs)
	//rc := len(rs)
	// if start > rc-1 || start < 0 {
	// 	start = 0
	// }
	// last := start + limit
	// if last > rc || last < start {
	// 	last = rc
	// }
	// qrs = rs[start:last]
	qrs = rs
	rt = "success"
	return
}
func (cold) Add(rec model.Cold) (rt string) {
	db, err := bolt.Open(model.Cfg.Boltdb, 0666, nil)
	if err != nil {
		rt = "数据库打开错误!"
		return
	}
	defer db.Close()
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("Savenum"))
		if err != nil {
			return err
		}
		ai, err := b.NextSequence()
		if err != nil {
			return err
		}
		rec.ID = ai
		ec, err := json.Marshal(rec)
		err = b.Put([]byte(strconv.FormatUint(ai, 10)), ec)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		rt = "数据库操作失败"
		return
	}
	rt = "success"
	return
}
func (cold) Auto(recs model.Auto) (rt string) {
	var rec model.Cold
	db, err := bolt.Open(model.Cfg.Boltdb, 0666, nil)
	if err != nil {
		rt = "数据库打开错误!"
		return
	}
	defer db.Close()
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("Savenum"))
		if err != nil {
			return err
		}
		ai, err := b.NextSequence()
		if err != nil {
			return err
		}
		rec = getcold(recs.Nop)
		rec.Nop = recs.Nop
		rec.ID = ai
		ec, err := json.Marshal(rec)
		err = b.Put([]byte(strconv.FormatUint(ai, 10)), ec)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		rt = "数据库操作失败"
		return
	}
	rt = "success"
	return
}

func getcold(recs string) (rec model.Cold) {
	var getcold = regexp.MustCompile(`开奖号码：<span class="fc-red">(.*?)</span>`)
	var getsecold = regexp.MustCompile(` 特别号：<span class="fc-red">(.*?)</span>`)
	// "http://www.fjtc.com.cn/Notice-3107?NO=18028"
	url := "http://www.fjtc.com.cn/Notice-3107?NO=" + recs
	resp, err := http.Get(url)

	if err != nil {
		fmt.Println("http get error.")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("http read error")
	}
	bodys := string(body)
	colds := getcold.FindAllStringSubmatch(bodys, -1)
	secolds := getsecold.FindAllSubmatch([]byte(bodys), -1)
	//
	cold := strings.Split(colds[0][1], " ")
	rec.D1 = cold[1]
	rec.D2 = cold[2]
	rec.D3 = cold[3]
	rec.D4 = cold[4]
	rec.D5 = cold[5]
	rec.D6 = cold[6]
	rec.D7 = cold[7]
	rec.Se = fmt.Sprintf("%s", secolds[0][1])
	return
}
